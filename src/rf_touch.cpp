#include <Arduino.h>

#include "rf_touch.h"

static constexpr int dispatch_cycle_time_ms = 100;
static constexpr int threshold_inactive = 0;
static constexpr int filter_period = 10;


WiFiUDP *RfTouch::s_udp = nullptr;
String RfTouch::s_host;
bool RfTouch::m_sendEnable = false;
uint8_t RfTouch::s_adjustThreshold = 20;

uint16_t RfTouch::s_pad_last_value[TOUCH_PAD_MAX];
uint8_t RfTouch::s_pad_threshold_percent[TOUCH_PAD_MAX];
bool RfTouch::s_pad_enabled[TOUCH_PAD_MAX];
bool RfTouch::s_pad_is_pressed[TOUCH_PAD_MAX];
uint16_t RfTouch::s_pad_filtered_value[TOUCH_PAD_MAX];
uint16_t RfTouch::s_pad_threshold[TOUCH_PAD_MAX];
RfTouchCallback RfTouch::s_pad_callback[TOUCH_PAD_MAX];



RfTouch::RfTouch() : event_timer{}
{   
    // Initialize touch pad peripheral, it will start a timer to run a filter
    touch_pad_init();
    // If use interrupt trigger mode, should set touch sensor FSM mode at 'TOUCH_FSM_MODE_TIMER'.
    touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER);
    // Set reference voltage for charging/discharging
    // For most usage scenarios, we recommend using the following combination:
    // the high reference valtage will be 2.7V - 1V = 1.7V, The low reference voltage will be 0.5V.
    touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
    //init RTC IO and mode for touch pad.
    for (int i=0; i<TOUCH_PAD_MAX; ++i) {
        s_pad_enabled[i] = false;
        s_pad_is_pressed[i] = false;
        s_pad_threshold[i] = threshold_inactive;
        // Inizialization using the default constructor of std::function
        s_pad_callback[i] = {};
    }
}

RfTouch::~RfTouch() {
    event_timer.detach();
}

void RfTouch::setUdp(const String &host, WiFiUDP *udp)
{
    s_udp = udp;
    s_host = host;
}

void RfTouch::enableSend(bool enable)
{
    m_sendEnable = enable;
}

void RfTouch::configure(const int input_number, const uint8_t threshold_percent, RfTouchCallback callback) {
    s_pad_enabled[input_number] = true;
    s_pad_threshold_percent[input_number] = threshold_percent;
    s_pad_callback[input_number] = callback;
}

void RfTouch::setThreshold(const int input_number, const uint8_t threshold_percent)
{
    s_pad_threshold_percent[input_number] = threshold_percent;
}

void RfTouch::setAdjustThreshold(uint8_t adjustThreshold)
{
    s_adjustThreshold = adjustThreshold;
}

void RfTouch::calibrate_thresholds() {
    uint16_t touch_value;
    for (int i=0; i<TOUCH_PAD_MAX; ++i) {
        if (s_pad_enabled[i]) {
            //read filtered value
            touch_pad_read_filtered(static_cast<touch_pad_t>(i), &touch_value);
            s_pad_threshold[i] = static_cast<uint32_t>(touch_value) * s_pad_threshold_percent[i] / 100;
            s_pad_last_value[i] = touch_value;
        }
    }
}

void RfTouch::begin() {
    for (int i=0; i<TOUCH_PAD_MAX; ++i) {
        if (s_pad_enabled[i]) {
            touch_pad_config(static_cast<touch_pad_t>(i), threshold_inactive);
        }
    }
    // Initialize and start a software filter to detect slight change of capacitance.
    touch_pad_filter_start(filter_period);
    touch_pad_set_filter_read_cb(filter_read_cb);
    // Set threshold
    calibrate_thresholds();
    event_timer.attach_ms(dispatch_cycle_time_ms, dispatch_callbacks, this);
}

void RfTouch::diagnostics() {
    for (int i=0; i<TOUCH_PAD_MAX; ++i) {
        if (s_pad_enabled[i]) {
            Serial.print("Button no.: "); Serial.print(i);
            Serial.print(F("  Current sensor value: "));
            Serial.print(s_pad_filtered_value[i]);
            Serial.print(F("  Threshold: "));
            Serial.println(s_pad_threshold[i]);
        }

    }

} // void diagnostics()



void RfTouch::filter_read_cb(uint16_t *raw_value, uint16_t *filtered_value) {
    for (int i=0; i<TOUCH_PAD_MAX; ++i) {
        s_pad_filtered_value[i] = filtered_value[i];
    }
}

void RfTouch::dispatch_callbacks(RfTouch* self) {
    for (int i=0; i<TOUCH_PAD_MAX; ++i) {
        if (s_pad_enabled[i]) {
            if(s_udp && m_sendEnable) {
                s_udp->beginPacket(s_host.c_str(), 55555);
                s_udp->printf("touch,pad=%d filtered_value=%d,threshold=%d\n", i, s_pad_filtered_value[i], s_pad_threshold[i]);
                s_udp->endPacket();
            }

            bool new_button_state = s_pad_filtered_value[i] < s_pad_threshold[i];
            // Transition from off to on state detected.
            // The callbacks are only fired when a button state actually changes
            // in order to avoid repeat presses.
            if(new_button_state && !s_pad_is_pressed[i]) {
                RfTouchCallback cb = s_pad_callback[i];
                if(cb) cb();
            }
            s_pad_is_pressed[i] = new_button_state;

            if(!new_button_state) {
                if( s_pad_filtered_value[i] > (s_pad_last_value[i] * (100 - s_adjustThreshold) / 100) ) {
                    s_pad_threshold[i] = s_pad_filtered_value[i] * s_pad_threshold_percent[i] / 100;
                    s_pad_last_value[i] = s_pad_filtered_value[i];
                }
            }
        }
    }
}
