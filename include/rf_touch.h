#ifndef _RF_TOUCH_H
#define _RF_TOUCH_H

#include <functional>
#include <driver/touch_pad.h>
#include <Ticker.h>

#include <WiFiUdp.h>

using RfTouchCallback = std::function<void(void)>;

class RfTouch
{
public:
    RfTouch();
    virtual ~RfTouch();
    void configure(const int input_number, const uint8_t threshold_percent, RfTouchCallback callback = nullptr);

    void setThreshold(const int input_number, const uint8_t threshold_percent);
    void setAdjustThreshold(uint8_t adjustThreshold);
    void calibrate_thresholds();
    void begin();
    void diagnostics();
    static void setUdp(const String &host, WiFiUDP *udp);
    static void enableSend(bool enable);

private:
    Ticker event_timer;
    static bool m_sendEnable;

    static uint8_t s_adjustThreshold;
    static WiFiUDP *s_udp;
    static String s_host;
    static uint8_t s_pad_threshold_percent[TOUCH_PAD_MAX];
    static bool s_pad_enabled[TOUCH_PAD_MAX];
    static bool s_pad_is_pressed[TOUCH_PAD_MAX];
    static uint16_t s_pad_filtered_value[TOUCH_PAD_MAX];
    static uint16_t s_pad_last_value[TOUCH_PAD_MAX];
    static uint16_t s_pad_threshold[TOUCH_PAD_MAX];
    static RfTouchCallback s_pad_callback[TOUCH_PAD_MAX];

    static void filter_read_cb(uint16_t *raw_value, uint16_t *filtered_value);
    static void dispatch_callbacks(RfTouch* self);
}; // class ReactiveTouch

#endif
